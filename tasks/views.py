from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm
from django.views.generic.edit import UpdateView

# Create your views here.


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            task = form.save()
            return redirect("show_project", task.pk)
    else:
        form = TaskForm()
    return render(request, "tasks/create.html", {"form": form})


@login_required
def list_task(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"task_list": tasks}
    return render(request, "tasks/list.html", context)


class TaskUpdateView(UpdateView):
    model = Task
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
