from django.urls import path
from tasks.views import create_task, list_task, TaskUpdateView

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", list_task, name="show_my_tasks"),
    path("<int:pk>/complete/", TaskUpdateView.as_view(), name="complete_task"),
]
